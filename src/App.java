import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        latihanScannerIdentitas();
    }

    /**
     * used for how to use java String
     */
     public static void belajarString() {
            String namaDepan = "Muhammad Salman";
            String namaBelakang = "Magribie";
            String fullName = "";
    
              // String concat #2
            fullName = namaDepan.concat("\s" + namaBelakang);
            System.out.println(fullName);
    
            // String concat #2
            fullName = namaDepan + "\s" +namaBelakang;
            System.out.println(fullName);

        }

        /**
         * used for how to use java Scaanner
         */
        public static void belajarScanner(){

            Scanner sc = new Scanner(System.in);
            System.out.print("Input username : ");
            String userName = sc.nextLine();
            System.out.println("Hello...." + userName);
        }

    /**
     * used for how to compile scanner
     */
    public static void latihanScanner() {

        Scanner sc = new Scanner(System.in);
        System.out.print("Input nama depan: ");
        String namaDepan = sc.nextLine();
        System.out.print("Input nama belakang: ");
        String namaBelakang = sc.nextLine();
        String fullName = namaDepan + "\s" + namaBelakang;
        System.out.println("Hello...." + fullName);
    }

    /**
     * used for how to compile identity
     */
    public static void latihanScannerIdentitas() {

        Scanner sc = new Scanner(System.in);
        System.out.print("Input NIK: ");
        String NIK = sc.nextLine();

        System.out.print("Input nama lengkap: ");
        String namaLengkap = sc.nextLine();

        System.out.print("Input tempat tanggal lahir: ");
        String tempatTanggalLahir = sc.nextLine();

        System.out.print("Input jenis kelamin: ");
        String jenisKelamin = sc.nextLine();

        System.out.print("Input alamat: ");
        String alamat = sc.nextLine();

        System.out.print("Input RT/RW: ");
        String rtRw = sc.nextLine();

        System.out.print("Input kelurahan/desa: ");
        String keluharanDesa = sc.nextLine();

        System.out.print("Input kecamatan: ");
        String kecamatan = sc.nextLine();

        System.out.println("Data user:" + "\n" + "NIK: " + NIK);
        System.out.println("Nama Lengkap: " + namaLengkap);
        System.out.println("Tempat Tanggal Lahir: " + tempatTanggalLahir);
        System.out.println("Jenis Kelamin: " + jenisKelamin);
        System.out.println("Alamat: " + alamat);
        System.out.println("RT/RW: " + rtRw);
        System.out.println("Kelurahan/Desa: "+ keluharanDesa);
        System.out.println("Kecamatan: " + kecamatan);
    }
}
